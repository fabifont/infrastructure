# infrastructure

## guidelines
1. Create a docker network that will be used by all the containers: `docker network create infrastructure`
2. Create credentials for traefik dashboard basicAuth: `echo $(htpasswd -nb user password) | sed -e s/\\$/\\$\\$/g > users_credentials`
3. Create `acme.json` to store certificates: `touch acme.json && chmod 600 acme.json`
4. Copy `.env.template` and set environment variables: `cp .env.template .env`
5. `.env`:
    - `MY_DOMAIN`
    - `CF_API_EMAIL`: cloudflare api email
    - `CF_API_KEY`: cloudflare api key
    - `TZ`: timezone
6. Check that `docker-compose.yml` is correct: `docker-compose config`

## services
- `portainer` at `portainer.YOUR_DOMAIN` (the first time you run it you must create the admin user, maybe do it in localhost before exposing it. You can't use both basicAuth and portainer auth: https://github.com/portainer/portainer/issues/3893)
- `traefik dashboard` at `traefik.YOUR_DOMAIN`
